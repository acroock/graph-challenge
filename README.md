# Graph Challenge

This challenge is somewhat open-ended, meaning there are no strict marking criteria.
That said, we are mainly looking for:
* Correctness
* Good software development practices (version control, functions/methods, tests, package management)
* Interesting ideas/solutions

You may use any language that you are familiar with, although Go, Python or SQL are preferred.

You are free to use any open source libraries you want, as long as they are specified in an appropriate way for the language you are using (e.g. a Python solution should have a `requirements.txt` file or similar).

There are also some bonus tasks below. I suggest you try at least one or two of them to give us an idea of what you're interested in.

## Steps
1. Fork this repo
2. In the `data` directory, you will find a JSON and a TXT file. They both represent the same graph in slightly different formats. You may use whichever you prefer for this challenge.
3. Write a program that reads the graph representation from a data file, builds a DAG and then outputs the topological ordering of the graph (Google is your friend if you don't know what that means).  
If you find there are cycles in the graph, you may break one of the graph edges to make the graph acyclic.  
The motivation is to be able to visit each node sequentially before any nodes that depend on that node.
4. Make sure to commit and push your work to your forked Git repo.
5. Write another program that takes a starting node ID, and outputs all the nodes that depend on the given node, recursively.  
For example given the simple graph `D -> C -> B -> A`, starting at node `A` the program should output `B, C, D`.

## Bonus tasks

You don't need to do all of these, but we might be impressed if you do.

### PostgreSQL
Create a simple DB schema to represent the graph. It should be a reasonably normalised set of SQL tables.

For an extra bonus, implement Step 5 above using Recursive CTEs in SQL (e.g. PostgreSQL).

### Docker
Write a Dockerfile appropriate for the language you chose that encapsulates all your source code and installs the required language/OS dependencies.

### Visualisation
Figure out a way to visually represent the graph so someone can easily see the downstream dependencies of a node.  
Keep in mind that for large graphs, a simple visualisation can become far too complicated and unreadable to be useful...

### CI pipeline
If you are familiar with Gitlab pipelines, configure a pipeline that runs your tests.
Then if the tests pass, write another job that runs your code on the sample data and exports the results as artifacts.
